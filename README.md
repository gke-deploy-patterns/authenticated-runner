# GKE Authenticated runner

Provisions Gitlab runner on GCE with configuration to pre-authenticate `kubectl` for a build.

The runner uses a Docker volume plugin (feeder) to run `gcloud container clusters get-credentials` in an isolated volume. Service account credentials can be safely stored (and/or rotated) an abstracted from the source Gitlab project.

## Install

1. Open Google Cloud shell
2. Clone this project

```bash
git clone https://gitlab.com/gke-deploy-patterns/authenticated-runner.git
cd authenticated-runner
```

3. Run terraform container

```bash
docker run -it --rm -v $PWD:$PWD -w $PWD hashicorp/terraform
```