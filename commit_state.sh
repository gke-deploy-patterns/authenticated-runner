#!/bin/sh

STATEFILE_PATH="${STATEFILE_PATH:-terraform%2Etfstate}"
STATEFILE="${STATEFILE:-terraform.tfstate}"
curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --request PUT "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/files/$STATEFILE_PATH?branch=$CI_COMMIT_REF_NAME&encoding=base64&commit_message=updating%20state" --data-urlencode "content=$(cat $STATEFILE | base64)"
