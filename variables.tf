variable "ssh_public_key" {}
variable "ssh_private_key" {}
variable "gitlab_project_id" {default="1"}
variable "deploy_account_json" {}
variable "runner_registration_token" {}