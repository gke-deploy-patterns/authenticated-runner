provider "google" {
}

data "google_compute_image" "coreos" {
  family  = "coreos-stable"
  project = "coreos-cloud"
}

data "ignition_user" "core" {
  name = "core"
  ssh_authorized_keys = [
    "${var.ssh_public_key}"
  ]
}

data "ignition_config" "runner" {
  users = [
    "${data.ignition_user.core.id}",
  ]
}

resource "google_compute_instance" "runner" {
  name         = "kubectl-runner"
  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.coreos.self_link}"
    }
  }

  network_interface {
    network = "default"
  }

  metadata {
    user_data = "${data.ignition_config.runner.rendered}"
  }

  connection {
    user        = "core"
    private_key = "${var.ssh_private_key}"
  }

  provisioner "file" "feeder" {
    source      = "feeder"
    destination = "/home/core/feeder"
  }

  provisioner "file" ".feeder.yaml" {
    source      = ".feeder.yaml"
    destination = "/home/core/.feeder.yaml"
  }

  provisioner "file" "deploy_key" {
    destination = "/home/core/keys/${var.gitlab_project_id}/key.json"
    content = "${var.deploy_account_json}"
  }

  provisioner "file" "deploy_env" {
    destination = "/home/core/envs/${var.gitlab_project_id}/project.env"
    content = 
<<EOF
GKE_CLUSTER_NAME=cluster-0
GKE_CLUSTER_ZONE=us-central1-a
EOF
  }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/core/feeder",
      "docker volume create --driver feeder kubeconfig",
      "docker run --name gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock:ro gitlab/gitlab-runner:alpine",
      <<EOF
docker exec gitlab-runner gitlab-runner register --non-interactive \
  --url "https://gitlab.com" \
  --registration-token "${var.runner_registration_token}" \
  --tag-list "kubectl,kubeconfig" \
  --executor docker \
  --docker-image "devth/helm" \
  --docker-volumes "kubeconfig:/root/.kube" \
  --pre-build-script "TIMES=0; while [ ! -f \"/root/.kube/config\" ]; do if [ \"\$TIMES\" == \"10\" ]; then exit 1; fi; echo \"Waiting for kube config... (attempt \$(( TIMES ++ ))/10)\"; sleep 5; done"
EOF
    ]
  }
  
}